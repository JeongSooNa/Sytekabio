# Linux OS Network edit

## Ubuntu
- [7Zip](https://www.7-zip.org/download.html)
- [VMware Ubuntu 네트워크 설정1](https://blog.naver.com/jjm1411/222223083695)
- [VMware Ubuntu 네트워크 설정2](https://blog.naver.com/jjm1411/222225270599)
- 위 설정을 통해 같은 네트워크의 다른 서버, IP와 통신 가능.
## CentOS
- [VMware CentOS 네트워크 설정](https://nirsa.tistory.com/15)
- CentOS 환경에서 네트워크 설정 과정에 있어서 방화벽 및 설치 문제.
- 해결하면 VMware의 가상서버 둘 (Ubuntu to CentOS) 연결이 가능할듯!

#### !
- VMware 가상서버가 켜져있다면 내 PC(mobaxterm)에서도 서버 접속이 가능하다!