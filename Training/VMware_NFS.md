# VMware Linux 환경에서 NFS서버 구현

### 필요사항
1. Vmware 설치 및 네트워크 환경 구현
2. Linux CentOS-7, Ubuntu-16.04 설치 및 서버 생성
3. 생성된 서버의 네트워크 설정

### NFS (Network File System)
- CentOS-7 : NFS-Server
    - 192.168.209.3 (본사)
    - 192.168.64.4 (AI센터)
- Ubuntb-16.04 : NFS-Client
    - 192.168.209.4 (본사)
    - 192.168.64.3 (AI센터)
- Server의 저장공간을 Client가 마운트하여 사용.
- 네트워크 환경을 설정했기 때문에 mobaxterm(내 PC)에서도 접속 및 사용이 가능.

### NFS-Server
- Linux CentOS-7에서 구현
1. nfs 설치
- rpm package 없을 시 설치
> sudo rpm -qa | grep nfs  
> sudo yum install nfs-utils

2. 공유할 폴더 지정 리스트 확인
> sudo vi /etc/exports
3. exports 내에 /share라는 폴더 리스트에 넣기 (vi editor내에서 작성)
    - rw : read, write 권한 설정
    - sync : 실시간 동기화 설정  
> /share 192.168.209.*(rw,sync)

![jpg](./img/nfs_1.PNG)

4. / 아래에 share 디렉토리 생성
> mkdir /share
5. 디렉토리에 권한 부여
> chmod 707 /share  
- 707 권한????
    - 1 (excution), 2 (write), 4 (read) 의 0~7까지의 조합.
    - 자리마다 각각 소유자의 권한/소유자가 속한 그룹의 권한/다른 사용자의 권한 의미
    - ex) 707 : xwr/---/xwr 이런 구조로 권한을 명령

![jpg](./img/nfs_2.PNG)

6. exportfs의 수정 내용 반영

> sudo exportfs -r

![jpg](./img/nfs_3.PNG)

7. NFS 서비스 가동
> sudo systemctl start nfs-server  
> sudo systemctl enable nfs-server
8. 방화벽 끄기
> sudo service firewalld stop

![jpg](./img/nfs_4.PNG)

9. 마운트, 공유가 잘 되었는지 확인
- test file을 만들어서 확인해 보고 나중에 Clients 서버에도 같은 file이 있나 확인하기!
> showmount -e  
> sudo exportfs -v

![jpg](./img/nfs_5.PNG)



### NFS-Client
- Linux Ubuntu-16.04에서 구현
1. nfs client package install
> sudo apt-get -y install nfs-common cifs-utils
2. nfs server에 마운트
> sudo mount -t nfs 192.168.209.3:/share /home/na/naClient  
- -t nfs : system type을 nfs로 설정
- 192.168.209.4:/share : nfs서버의 ip와 mount할 디렉토리 지정
- /home/na/naClient : nfs서버와 mount될 디렉토리 지정
3. nfs server mount 확인
> sudo df -h 192.168.209.3:/share /home/na/naClient

![jpg](./img/nfs_6.PNG)

4. mount 해제
> sudo umount /home/na/naClient

### Result
- NFS-Server 확인
- /share/test.txt

![jpg](./img/nfs_7.PNG)

- NFS-Client 확인
- /home/na/naClient

![jpg](./img/nfs_8.PNG)


### 재접속
- system out 후 다 시접속할 때
- NFS-server 방화벽 끄기  
> sudo service firewalld stop  
- NFS-client mount  
> sudo mount -t nfs 192.168.64.4:/share /myShare  

### Reference
- [CentOS 7 Linux NFS 구축하기](https://ansan-survivor.tistory.com/687)
- [Ubuntu NFS Client 설치 및 설정하기](https://server-talk.tistory.com/378)