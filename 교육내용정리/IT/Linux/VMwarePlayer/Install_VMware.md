# Download VMware installer
***
- VMware 홈페이지에서 Download
- [VMware install](https://www.vmware.com/kr/products/workstation-player/workstation-player-evaluation.html)
- 실행
***
- 실행 시 간혹 Microdoft VC 재배포 패키지 설치로 인해 재부팅을 해야 하는 경우가 있습니다. 재부팅을 하지 않으면 더 이상 진행이 되지 않으니 아래와 같은 창이 나타난다면 "예"를 클릭하여 재부팅을 한 후 설치를 이어가시면 됩니다.
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2F3kuBj%2FbtqKnpokseo%2F6S3CKp0hGjmwHW7iyGIRIK%2Fimg.png)
***
- Next를 눌러 진행
![jgp](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbEilUO%2FbtqKrLYdrwM%2FNM8YiBoHfrpstJrjf7qRnK%2Fimg.png)
***
- 라이센스 동의 후 Next
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FtE0bX%2FbtqKlTQ4SHP%2FAQPHSmBxKby80WKaE7Cwx0%2Fimg.png)
***
- Next
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2Fb4UQ2w%2FbtqKrqGNy7U%2FjFDn6W9dwWJkNzVTQzPhSk%2Fimg.png)
***
- Check 여부? (우선 Check Box 해제 후 Next)
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FJUrT2%2FbtqKkPuwX72%2FkulP1E9HK8gz3uK3l0wZsK%2Fimg.png)
***
- 바로가기 아이콘, 시작메뉴 아이콘 설정
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FmHfiA%2FbtqKqydZgxS%2FAgoxhgMk6XH46frIkxgzm1%2Fimg.png)
***
- install
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbtEr1x%2FbtqKlT4vVJJ%2FqAJUrtc4Xsz91LlKKh3KV0%2Fimg.png)
***
- 설치 완료.
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbEXxQD%2FbtqKoqgdYzp%2Fqc8Wzv6hInlmDsYKyYjcZK%2Fimg.png)
***
- VMware 실행
- 무료버전 사용을 위해 Continue
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FdLm1kt%2FbtqKmp9ZABI%2FmRMkZ2JyRpdHZIq1EnVoN1%2Fimg.png)
***
- Finish
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbJ9ZL0%2FbtqKjbEBDaH%2FqnlJYNxeJcu8YzMKQwkhJk%2Fimg.png)
***
- 완료!

#### referance
- [항상 배우자](https://catnip-archive.tistory.com/entry/VMware-VMware%EC%97%90-%EA%B0%80%EC%83%81%EB%A8%B8%EC%8B%A0-%EC%B6%94%EA%B0%80%ED%95%98%EA%B8%B0feat-Ubuntu-1804-LTS)