# Linux OS install

- VMware에서는 window, Ubuntu, CentOS 등 운영체제를 가상 머신으로 추가 가능.
- 먼저 이미지(iso file)가 필요.
***

### iso img file download 
- [CentOS-7-x86_64-Minimal-2009.iso Download](https://mirror.kakao.com/centos/7/isos/x86_64/)
![jpg](../img/VMware_linux_iso.PNG)
***
- [ubuntu](https://ubuntu.com/)  
- [ubuntu 16.04](https://releases.ubuntu.com/16.04/)  
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbnHzo8%2FbtqKMyS80jw%2FCjYqukrBD4FfFzw8dW6Me0%2Fimg.png)
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FvEecL%2FbtqKPTCiBII%2F8EakWrWzQdvwUpRtlTFZx1%2Fimg.png)

***
### Create a New Virtual Machine
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2Fbp6qFp%2FbtqEnJm1bSv%2F2ATrh2pGFQzknJ5BKKSua0%2Fimg.png)
***
- CentOS-7, Ubuntu-16.04 각각 배포판 iso file 선택
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2F6aAXG%2FbtqEqtbJl1e%2FuseYXkCrKhZkW8kPHaBBJk%2Fimg.png)
- Next, 만약 운영체제를 선택하라는 창이 뜨면 linux 선택 후 Next
***
- 가상 머신 이름 설정  
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2Fc7Op7R%2FbtqEq61ETop%2FRcO6Hn087S6UJkJl3qjNTk%2Fimg.png)
***
- 디스크 용량 활성화
- 두번째, 파일을 여러 개로 분할하여 저장할 경우 처리속도가 비교적 빠르나 그냥 진행하여도 큰 상관은 없다.
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FbVPrJX%2FbtqEoBPwXiJ%2FCZHr445JKRrNCzoFKwWyB0%2Fimg.png)
***
- 설치 설정 확인 및 Finish  
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2Fv4f0X%2FbtqEpZ9OZWk%2FenPi72ZQKWLkvCMkgnNHs1%2Fimg.png)
***
- 설치 완료 후 Play virtual machine
![jpg](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FddFZSG%2FbtqEqsw8v28%2FZF8S04pQM7hwRCTGsGv9pk%2Fimg.png)
***
- 각 환경에 맞는 언어, user 및 password 설정, 디스크 용량 설정 등 환경 구성 후 사용.
- 파티션 Auto로 설정, Root암호 및 사용자 생성
- 설치 완료 후 재부팅한 후 terminal을 찾아 실습을 진행하면 된다.


#### referance
- [항상 배우자](https://catnip-archive.tistory.com/entry/VMware-VMware%EC%97%90-%EA%B0%80%EC%83%81%EB%A8%B8%EC%8B%A0-%EC%B6%94%EA%B0%80%ED%95%98%EA%B8%B0feat-Ubuntu-1804-LTS)
- [kshman94](https://kshman94.tistory.com/66)