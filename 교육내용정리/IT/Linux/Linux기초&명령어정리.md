# 리눅스(LINUX) 기초 & 명령어
```
GUI : 아이콘으로 제어
CLI (Command Line Interface) : 명령어로 제어
내리는 명령은 현재 머무르고있는 디렉토리를 대상으로 이루어진다.
(어떤 디렉토리에 머무르고 있는지를 확인.)

명령어 뒤 ( - ) : parameter, 옵션
```
***

# Linux 기초
***
## CPU
- 중앙처리장치 : 기억, 해석, 연산, 제어
```
cat /proc/cpuinfo : CPU정보 상세 확인
```
![jpg](./img/linux_command_cpu_1.PNG)
- processor : 0부터 시작하는 각 프로세서의 고유 식별번호
- model name : 모델명, 클럭속도 확인 가능.
- siblings : Tread로 core 갯수와 비교하여 하이퍼스레딩 여부 확인.
- cpu cores : 물리적인 코어의 갯수.
- cpu의 성능에 따라 작업 처리 분량을 결정.
```
cat /proc/cpuinfo | grep processor | wc -l : 프로세스 갯수 확인
```
```
cat /proc/cpuinfo | grep 'core id' : 코어 갯수 확인
```
- grep? 특정 파일에서 지정된 내용을 찾는 명령어.
- wc? 수를 세는 명령어(-l : 행)(-w : 단어)(-c : 문자)
- uniq를 사용하면 중복제거를 할 수 있다. 다음과 같이 명령문을 작성하여 core의 갯수를 확인해보았다. (- c 옵션으로 갯수를 바로 확인도 가능)
![jpg](./img/linux_command_cpu_2.PNG)
![jpg](./img/linux_command_cpu_3.PNG)
- 프로세서(논리회로)는 8개, 코어는 4개로 하이퍼스레딩이 적용된 것을 알 수 있다.
***
## Memory
```
free : .proc/meminfo에서 메모리 정보를 가져온다. 쉽게 메모리 사용량과 여유량, 캐싱으로 사용되는 메모리가 얼마나 있는지 파악.
free -option : -b,k,m,g(단위별) / -h(human:크기에 따라 보기 쉽게 단위 표시) / -t(total:memory와 swap의 합계도 표시)
```
![jpg](./img/linux_command_free_1.PNG)
- total : 설치된 총 메모리 크기 / used : 사용중인 메모리 크기 / free : 사용되지 않은 메모리 크기
- shared : 여러 프로세스에서 공유되는 메모리 크기 / buffer/cache : 버퍼와 캐시를 더한 사용중인 메모리(성능 향상을 위해 사용 중)
- avilable : swapping 없이 새로운 프로세스에서 할당 가능한 메모리의 예상 크기
- swap? RAM의 용량이 차면 swap 메모리가 늘어나도록 되어있다. (디스트 공간에서 가상 메모리로 저장공간을 대체.) > 속도 저하.
![jpg](./img/linux_command_free_2.PNG)
***
## Disk
```
df : 디스크의 남은 용량 확인 [옵션 k(킬로바이트단위),m(메가바이트단위),h(깔끔)]
(Disk Free)
```
![jpg](./img/linux_command_df_1.PNG)
- Filesystem(파일시스템) / Size(디스크크기) / Used(사용된용량)
- Available(사용가능용량) / Use%(사용된용량비율) / Mounted on(마운트된 지점)
- 여기서 마운트란? 디스크와 같은 장치를 특정 디렉토리에 연결시켜주는 것.
- 현재 디렉토리의 디스크, 용량, 사이즈 확인 : [du](#check-directory-and-file-size)
- 파일 시스템? 운영체제가 파일, 디렉토리를 효율적으로 관리하기 위한 트리구조 시스템.
- Linux는 파일로 이루어져 있어 디스크와 같은 물리적인 저장소를 파일시스템으로 디렉토리와 연결해주어 사용.
***
## IP
```
hostname -I : IP 하나만 확인
ifconfig : IP(inet), 서브넷마스크(netmask), 게이트웨이(broadcast) 등 확인
ip addr show : IP(inet), 서브넷마스크(netmask), 게이트웨이(brd) 등 확인
    서브넷 마스크는 IP주소 뒤 (/16)로 16bit인 255.255.0.0을 사용하는 것을 알 수 있다.
    > inet 10.10.8.73/16 brd 10.10.255.255 
```
![jpg](./img/linux_command_ip_1.PNG)

***
# 기본적인 명령어 및 실습 실행
***
## 시작과 종료
```
shutdown -P now
halt -p
init 0 : 시스템 종료
```
```
shutdown -r now
reboot
init 6 : 시스템 재부팅
```
```
logout
exit : 로그아웃
```
- init 명령어 뒤의 숫자를 런 레벨(Runlevel)이라고 한다.
```
0 : Power off(종료 모드)
1 : Rescue(시스템 복구 모드), 단일 사용자 모드
3 : Multi-User, 텍스트 모드의 다중 사용자 모드
5 : Graphical, 그래픽 모드의 다중 사용자 모드
6 : Reboot, 재부팅
2,4는 다중사용자 의미는 있으나 사용하지 않는다.
```
![jpg](./img/linux_command_runlevel_0.PNG)
![jpg](./img/linux_command_runlevel_1.PNG)
![jpg](./img/linux_command_runlevel_3.PNG)
![jpg](./img/linux_command_runlevel_5.PNG)
![jpg](./img/linux_command_runlevel_6.PNG)
***
## 가상 콘솔 환경
- CentOS는 총 6개의 가상 콘솔을 제공한다.
- Ctrl + Alt + F1~F6 까지로 가상 콘솔 이동 가능.
- F1은 xWindow 모드
***
## 자동완성 / 히스토리
- Tab 을 이용하여 디렉토리, 파일을 자동완성 및 목록 확인을 할 수 있다.
- 방향키를 이용하여 이전 입력 명령어를 조회할 수 있다.(도스키)
```
history : 이전 명령어 순차적으로 출력 (head,tail을 사용해 처음, 마지막 조회 내용을 확인할 수 있다.)
```
## vi Editor
```
vi file_name : 을 통해 열기
```
![jpg](./img/linux_command_vi_1.png)
- [vi editor](./vi%ED%8E%B8%EC%A7%91%EA%B8%B0.md)
- 입력모드(작업)와 명령(ex)모드(저장,종료,취소 등)로 나뉘어진다.
- vi는 자주 사용해야 할 기능으로 반드시 익혀야한다.
***

## 마운트
- 디스크와 같은 물리적 장치를 특정 위치(디렉토리)에 연결해주는 명령어.
- 리눅스의 파일 시스템 구조는 다음과 같이 구성되어 있는데, 초기에는 아래와 같이 cmt디렉토리에 아무것도 없다.
![jpg](./img/linux_command_mnt_2.PNG)
![jpg](./img/linux_command_mnt_1.PNG)
- 따라서 마운트를 이용해 디렉토리를 장치와 연결시켜주는 과정이 필요.
- CD, DVD, USB와 연결 (xWindow/text모드에서 사용법 익히기)
***

## Move directory
```
cd ~ : 디렉토리 이동
```
- 경로 : 파일/디렉토리를 찾아가는 방법
1. 절대경로 : 최상위 디렉토리를 기준으로 기입 (/user/desktop)
2. 상대경로 : 현재 디렉토리를 기준으로 기입 (na/na_github/syntekabio/)
- 차이점 : 단순히는 맨 앞에 / 가 붙느냐이지만 개념을 이해해야한다.
- / : 최상위 디렉토리
- ./ : 현재 디렉토리
- ../ : 상위 디렉토리
- ex) 현재 경로(/home/mobaxterm/Desktop/na/GitHub)에서 다른 경로(/home/mobaxterm/Desktop/test/test.txt) 찾아가기
![jpg](./img/linux_command_cd_1.PNG)
![jpg](./img/linux_command_cd_2.PNG)

***
## Check Directory
```
pwd : 현재 경로 확인
```
![jpg](./img/linux_command_pwd_1.PNG)
***
## Check Directory and File size
```
du : 현재 경로 및 하위 디렉토리 사이즈 확인
du -a : 현재 경로 및 하위 디렉토리 사이즈 + 디렉토리 내 파일 사이즈 확인
du -h : 사이즈 (KB/MB/GB)단위로 표기
du -ah : 디렉토리, 파일 사이즈 단위로 확인
du -s : 디렉토리 전체(/) 사용량 표시
```
![jpg](./img/linux_command_du_1.PNG)
![jpg](./img/linux_command_du_2.PNG)
![jpg](./img/linux_command_du_3.PNG)
![jpg](./img/linux_command_du_4.PNG)
***
## Find file
```
find . -name "~" : 이름으로 파일 찾기
find . -type ~ : 타입[d(디렉토리), f(파일), l(심볼릭링크)]으로 파일 찾기
find . -empty : 빈 디렉토리나 크기가 0인 파일 찾기
find . -size ±~ : size로 찾기
```
![jpg](./img/linux_command_find_2.PNG)
![jpg](./img/linux_command_find_1.PNG)
## 새 디렉토리 생성(Create Directory)
```
mkdir ~ : 현재 경로에 새 디렉토리 생성
mkdir -p ~/~/~ : 부모 디렉토리 생성
```
***
## Create File
```
touch ~.txt : txt 파일생성
```
- 이미 존재하는 경우 수정시간을 변경
***
## Check list in directory
```
ls : 현재 경로의 파일 확인
ls -l : 현재경로의 파일 상세 확인(권한 확인도 가능.)
ls -a : 감춰진 파일 확인
ls -al : 파일 모두 상세확인
ls -lh : 상세확인+사이즈 확인
ls -lSh : 사이즈 순으로 정렬
```
- 파일 권한 확인
    - 기본적으로 -rwx r-x r-x 로 구성.
    - 사용자 권한, 그룹 권한, 다른 사용자 권한.
    - Read(읽기), Write(작성), eXecute(실행)
![jpg](./img/linux_command_ls_1.PNG)
- 파일 앞에 .이 붙어있으면 감추어진 파일
- ex) -rw-rw-rww. 1 na na 43 Aug 10 12:01 test1.txt
- 구성 : 파일종류및권한 링크수 사용자 그룹 파일크기 수정시간 파일이름
***
## Delete
```
rm file_name : 삭제 (file)
rm -r dirctory_name : 삭제 (directory)
rmdir dirctory_name : 삭제 (directory) 단, 비어있어야 함.
```
- rm -r : 모든 디렉토리가 삭제될 수 있어 조심!
***
## Help
```
~ --help : 명령어 도우미
man ~ : 명령어에 대한 사용 설명서 (/text : text내용 확인, q : 나가기)
```
***
## Move
```
mv name to_name : 파일, 디렉토리 이동 (뒤에 file명을 입력할 경우 이름 변경)
```
![jpg](./img/linux_command_mv_2.PNG)
***
## head / tail
```
head ~ : ~의 앞 10행 출력
tail ~ : ~의 마지막 10행 출력
```
***
## more
```
more ~ : ~을 화면에 페이지 단위로 출력
less ~ : more보다 기능이 더 확장된 명령어 (q로 종료)
```
***
## file
```
file file_name : file이 어떤 종류의 file인지 표시
```
***
## Copy
```
cp file_name to_directory_name : 파일 복사
cp -r directort_name to_directopry_name: 디렉토리 복사
```
![jpg](./img/linux_command_cp_1.PNG)
- 위와 같이 여러 개의 파일을 이동, *.확장자명 을 이용하여 모두 이동, 디렉토리 이동이 가능하다.
***
## Copy to Other Server
- 원격지 파일 복사
```
scp user@10.10.111.111:/home/desktop/test.txt /home/desktop/ : 다른 서버의 test file을 로컬 서버의 저장소로 복사
scp /home/desktop/test.txt user@10.10.111.111:/home/desktop/ : 반대로도 가능
```
- 다른 PORT를 사용한다면
```
- scp -P 8080 user@10.10.111.111:/home/desktop/test.txt /home/desktop/
```
위와 같이 -P 포트번호를 입력하여 사용한다.
***
## Process
```
ps : 현재 동작하는 프로세스 목록 확인
ps -f : 현재 동작하는 프로세스목록 상세 확인
ps -e : 모든 프로세스 리스트 확인
```
![jpg](./img/linux_command_ps_1.PNG)
- UID : userID / PID : 식별번호 / PPID : 부모 프로세스의 PID
- TTY : 콘솔, 터미널 / STIME : 시작시간 / COMMAND : 프로세스 실행 명령 행

***
## 설치, 다운로드(Install)
```
apt-get install git : git download (root 권한으로 실행하여야 함.)
sudo apt-get install git : root권한으로 설치
apt-get install
```
- package manager : 기본제공 이외에 install 하여 사용 가능
- Homebrew : 어떤 프로그램을 설치해주는 일종의 intaller(app store)
```
wget -O file_name url_name : 명령어 시스템에서 이미지, 파일 등 다운로드를 받는 프로그램(url사용)
```
***
## 파일 편집기(File Editor)
```
nano : 파일 편집기
vi : 파일 편집기(사용할 것!) 
```
- 편집기 사용법을 알고있으면 대부분의 unix환경에서 사용 가능.
[vi 편집기 정리](./%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md)

***
## Pipe
```
ls -l | grep ln : "ln"이 들어간 output만 출력
ls -l | grep ln | grep -v Micro : "ln"이 들어가고, "Micro"가 안들어가는 output만 출력
```
![jpg](./img/linux_command_pipe_1.PNG)
```
ls | sort : 해당 디렉토리에 있는 것을 정렬
```
![jpg](./img/linux_command_pipe_2.PNG)
- 위와 같이 pipe를 통해 input을 넣으면 주어진 명령어를 순차적으로 이행하고, output이 출력되게 된다.
- 여러가지 명령어를 복합적으로 적용할 때 사용!
***
## Redirection (재지향)
- 각종 명령어 출력 결과를 파일로 저장.
1. STDIN(Standard input) : 표준 입력
2. STDOUT(Standard output) : 표준 출력
3. STDERR(Standard error) : 표준 에러
- input / output 개념
- (>)을 통해 input, error 입력 가능
- (<)을 통해 output 출력 가능
```
echo hello > hello.txt : 파일에 작성한 문자 입력
ls -l > result.txt
rm text.txt 2> error.log
```
- IO Redirection (Input Output Redirection)
![jpg](https://peterdev.pl/assets/unix-process-diagram.svg)   
- output을 txt파일로 저장(>)
- /dev/nyll 하면 휴지통(낭떠러지?)으로 이동하여 출력x
- Standard Output을 저장하기 때문에 Standard Error는 저장하지 않는다.
- 위와 같이 (2>)로 Standard Error를 저장 가능.
```
ls -l >> result.txt
```
- output을 txt파일에 Append(>>)
```
echo ~ > ~.txt : 입력내용(input) 파일에 저장
```
![jpg](./img/linux_command_redirection_1.PNG)
- 위와 같이 input, output 제어
```
cat < hello.txt > hello_world.txt
```
- 위와 같이 COPY 로 이용할 수도 있다.
***
## 파일 확인(Check file)
```
cat file_name : file의 내용 확인 !!인자로 입력
cat : 입력한 text를 바로 출력
cat < file_name : !!Standard Input으로 입력
head -3 file_name : file의 3줄만 출력 (defaule = 10)
```
- cat 입력 후 바로 출력 시 ctrl+d 로 close
- 사용자가 입력한 text를 standard input, standard output으로 바로 출력
***
## 메일(E-mail)
```
mail email_address <<*
~
~
*: email 보내기
```
***
## 쉘(Shell)
- Shell VS Kernel
- 명령어를 Shell에서 해석하기 쉽게 변형하여 Kernel로 전달.


# User & Group
- Linux는 다중 사용자 시스템.
- 기본적으로 root라는 Super User가 존재해 모든 작업을 할 수 있는 권한이 있다.
- 모든 사용자는 하나 이상의 그룹에 소속되어 있다.
```
cat /etc/passwd : 사용자가 정의되어있는 파일
```
***
## 사용자 (user)
- 다중 사용자 시스템 : 여러 명이 시스템을 이용할 수 있다.
```
id : 현재 user의 정보 확인
who : 이 컴퓨터에는 누가 접속할 수 있는가.
exit : 로그아웃. 유저 접속 해제.
```
- 관리자(super user / root)와 일반 사용자(user)
- super user는 일반적으로 root, 명령어 입력창 뒤가 #
- 일반 user는 user_name, 명령어 입력창 뒤가 $
```
su : 관리자(super user / root)로 접속
su - root : 위와 동일 root로 접속
exit : 로그아웃. 유저 접속 해제.
```
- 몇몇 운영체제는 root사용자의 접근을 막아놨음.
```
sudo passwd -l root : root 사용자 lock
sudo passwd -u root : root 사용자 unlock
sudo command_name : 일시적으로 super user의 권한으로 실행.
```
- root의 접근은 lock 해놓는 것이 안전!
- root로 접속 후 명령하는 것 보다 sudo를 이용해서 command가 안전!
```
sudo useradd user_name : 사용자 생성 (sudo)권한으로 실행.
sudo useradd -m user_name : 사용자의 홈 디렉토리도 생성

tail -n 5 /etc/pqsswd : 생성된 사용자 확인(아래5줄만 보기위해 옵션 추가)
ls /home : 생성된 사용자 디렉토리 존재 확인

sudo passwd user_name : 사용자의 비밀번호 설정

sudo userdel -r user_name : 사용자 삭제. 디렉토리도 함께 삭제하기위해 -r
```
***
## 권한관리(Admin)
```

```