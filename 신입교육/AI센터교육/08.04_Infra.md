# 2022.08.04 이사님 인프라 교육

## STB-CLOUD CONTENTS(PROJSCT)
- 현재 클라우드 서비스 제작 중
1. 유전체
2. 단백질체(신약/화합물)


## 슈퍼컴
#### 슈퍼컴퓨팅(Super Computing)
- 분산처리(중요) : 여러 대의 슈퍼컴을 활용하여 나누어 작업 수행

## CPU (intel / AMD)
#### CORE
- 코어의 갯수에 따라 작업 수행 능력이 달라진다.
- ex) core(6*2) / memory(64GB) / disk(500GB) / NIC(2)

## 과정
```
    1. 인프라       2. 자동화       3. 스크립트     4. 툴(공용/로컬)
    서버(3천대)     - 스크립트 작업을 표준화하여           50 / 50
    - CPU            자동화 과정.                   - DeepMatcher
    - GPU               파이프라인(PipeLine)        - NEO-ARS(신생항원예측)
    - Storage                                      - NGS-ARS(유전체)
```

## 작업 (Scheduler)
1. Slum : 정밀한 작업(의료기관?)
2. Parallel : 빠르게 처리(일반적으로 사용)


## SEARCH LIST
#### NIC(Network Interface Card) 
- PC나 서버 등 컴퓨터를 네트워크에 연결시키기 위한 장치.
#### CPU & memory 
- CPU에서 프로그램 작업을 처리, memory에서 내용을 저장(임시)
- Memory에는 CPU의 프로세서에 위치한 레지스터(Register), CPU의 필요한 데이터를 우선적으로 제공하는 캐시(Cache), 주기억장치(RAM), 보조기억장치(HDD,SSD) 가 있다.
#### 유전체 
- genome(게놈), 한 개체의 유전자 및 외적인 부분까지 모두 포함하는 전체 염기서열. 한 생물종의 거의 완전한 유전정보.
#### 단백질체 
- 세포 내의 한 유전체에서 생성될 수 있는 단백질의 총합, 전체 / 단백질체 분석은 대부분 질량분석에 의존한다.