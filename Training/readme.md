# Training

1. Linux환경에서의 실습
    - VMware Linux CentOS-7 / Ubuntu-16.04 가상환경 구축
    - VMware Linux NFS-Server/Client 네트워크 환경 구축
    - Shell Script를 사용한 명령어 실행 및 Linux명령어 연습
2. Python Script를 사용한 실습
3. Deepmatcher 실습
4. Shell Script를 활용한 data handling