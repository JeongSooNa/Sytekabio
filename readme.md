 # Syntekabio

## 교육 목표
1. DMC(DeepMatcher) Contents 이해 및 결론 도출
    - pipeline 이해
    - Cloud Service 활용
2. Software 기초 습득 및 활용.
    - python
    - shell script
3. Super Computer Infra Network System 구조 이해
    - linux(CentOS-7 Ubuntu1-16.04)
    - Server(CPU GPU Storage)
4. AI Drug development Basic 이해
    - Bio Technology 기초
    - 전반적인 신약 개발 과정 이해

***
## 학습 내용 정리
1. [BT(Bio Technology)](https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/BT)
    - [Basic](https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/BT/Basic)
2. [IT(Information Technology)](https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/IT)
    - [CPU](https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/IT/CPU)
    - [Linux]https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/IT/Linux)
    - [Server](https://github.com/JeongSooNa/Sytekabio/tree/main/%EA%B5%90%EC%9C%A1%EB%82%B4%EC%9A%A9%EC%A0%95%EB%A6%AC/IT/Server)

***
## Training
- 교육기간 중 필요한 과제, 실습 모음
- [View Directory](https://github.com/JeongSooNa/Sytekabio/tree/main/Training)


***
## 교육 내용
- [2022.08.04 Infra 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/AI%EC%84%BC%ED%84%B0%EA%B5%90%EC%9C%A1/08.04_Infra.md) _ 정종철 이사님
- [2022.08.09 IT 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/AI%EC%84%BC%ED%84%B0%EA%B5%90%EC%9C%A1/08.09_IT.md) _ 원대희 선임님
- [2022.08.10 Linux 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/AI%EC%84%BC%ED%84%B0%EA%B5%90%EC%9C%A1/08.10_Linux.md) _ 원대희 선임님
- [2022.08.11 DMC 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/ADSP%EA%B5%90%EC%9C%A1/08.11_DMC.md) _ 신지윤 박사님
- [2022.08.11 NGS 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/AI%EC%84%BC%ED%84%B0%EA%B5%90%EC%9C%A1/08.11_NGS.md) _ 홍운영 선임님
- [2022.08.16 Linux 교육](./%EC%8B%A0%EC%9E%85%EA%B5%90%EC%9C%A1/AI%EC%84%BC%ED%84%B0%EA%B5%90%EC%9C%A1/08.16_Linux.md) _ 원대희 선임님


***
## 현재 진행상황
1. 네크워크 교육 및 기본적인 서버 구조 숙지.
2. Python Basic 과정 진행 중.
3. 신약개발과정 기본/단편적으로 이해.
4. Linux운영체제에서 기본적인 명령어 숙지, 가상서버를 이용한 네트워크 구성.
5. Linux운영체제에서 script를 이용한 실행 및 vi editor 사용 진행 중.
6. DeepMatcher Contents 이해 및 기초 단계 숙지 중.
***
 ## AI 신약 개발 운영!!
![jpg](./Synteka_file/img/AINDD_OP.PNG)
![jpg](./Synteka_file/img/%EC%BA%A1%EC%B2%98.PNG)
 
