# Server 서버
- 서비스를 제공.
- 서버마다 역할이 다름. 구조도 다르다.
- CPU, GPU, Storage 서버 등 CPU, HDD등 구성요소가 다르다. (나중에 보고 확인!)

### 서버 종류
1. Worker node
```
CPU(Central processing Unit) 작업서버 : 중앙처리장치, 정밀한 작업
GPU(Graphics processing Unit) 작업서버 : 나누어 처리, 성능 이점
```
2. Storage Server
- 저장
- input, output data 저장 기능.
- 여러 개의 하드디스크로 구성