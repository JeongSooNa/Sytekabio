# 2022.08.11 홍운영 선임님 NGS 교육

## 기초
```
cell(세포) > tissue(조직) > organ(기관) > organ system(기관계) > organism(개체)
```
- CELL (세포) : 한 개체를 이루는 최소 단위.
- Nucleus(핵) + 미토콘드리아
- Genome(유전체) : 한 개체의 유전정보, 전체 염기서열
- Chromosome(염색체) : 유전물질을 담고 있는 구조물
    - Gene : Proteins의 정보를 담고 있다.
- DNA(Deoxyribo Nucleic acid) : 유전정보를 담고 있는 화학 물질, 이중나선구조
    - A : Adenine / T : Thymine
    - C : Cytosine / G : Guanine
- Protein
    - 20개의 다른 유형의 아미노산
    - 다양한 function(기능)이 존재.
- 세포가 생성과 사망하는 사이클을 반복.
- Central Dogma : protein이 만들어지는 일련의 과정
    - DNA > Transcription > RNA > Translation > Proteins

## NGS-ARS
1. Sequence
    - Sanger Sequencing
    - NGS (Next Generation Sequencing)
2. Read trimming
    - Read : 다 읽을 수 없어서 유전정보를 쪼갠다.
    - Read length : 100~500bp
    - Quality score
    - Adaptor : 인위적으로 붙인다. (유전정보를 담고 있는 Exon만 추출 / 반대:Intron)
    - Trimming
3. Read mapping
    - reference : 유전자 위치 정보를 정리한 자료.()
    - 추출한 read가 reference의 어디에 존재하는지 찾아준다.
        - GRCh-37 > GRCh-38 / hg-18 > hg-38
        - 버전이 바뀌면서 reference 성능 향상.
4. Variant calling
    - SNV (Single Nucleotide Variant) : 해당 위치 정보다 변경.
    - InDel (Insertion or Deletion) : 해당 위치의 정보 추가 or 삭제
    - CNV (Copy Number Variation) : read(큰 지역)에 따라 복사됨.
    - Fusion

> Annotation : 변이가 어떤 기능을 하는가? (reference 기준)
> Filtering : Annotation을 보고 걸러내는 작업

## Genomics to Proteomics
- Computing 기술 발전으로 인한 가속화
- AI 신약 개발에 영향. (빅데이터, Deep Learning)

## ARS
- Auto Reporting System : 시스템 자동화.
- 시스템 클라우드 서비스 구성.