# LINUX 기초
## 2022.08.03 ~ 05
### 할 것!
1. CPU, Memory, Disk, IP 등 컴퓨터 구조 및 리눅스 구조 공부
2. 명령어 옵션, 용어, 경로체계(상대경로, 절대경로) 공부
3. 이사님 교육사항(인프라교육) 정리
# LINUX구조! 컴퓨터 구조! 옵션! 용어! 경로체계! 캐싱! 스와핑! 등등!
***
## 서버접속
1. CPU / Memory / Disk / Network(IP) 찾기위한 명령어
    1. [CPU](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#cpu)
    2. [Memory](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#memory)
    3. [Disk](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#disk)
    4. [IP](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#ip)
2. 주로 많이 사용하는 명령어 & 결과(캡처)
## 파일 위치 관련
1. [디렉토리 변경](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#move-directory)
2. [디렉토리 내용 리스트](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#check-list-in-directory)
3. [파일 사이즈, 디렉토리 사이즈 확인](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#check-directory-and-file-size)
4. [파일 찾기](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#find-file)

## Process
- 현재 동작하는 프로세스 확인 (정보의 의미, 옵션)
- [Process](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#process)

## USER
1. 생성
2. 삭제
3. 권한 변경 (권한의 의미, 파일 권한 변경)

## 파일 복사 / 이동
1. [파일 생성](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#create-file)
2. [파일 복사 명령 종류, 사용법, 결과](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#copy)
    - [cp 외에 redirection을 이용한 copy](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#redirection)
3. [파일 이동](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#move)
4. [원격지 파일 복사](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#copy-to-other-server)

## 파이프(Pipe), 리다이렉션(Redirection) 사용법
1. [Pipe](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#pipe)
2. [Redirection](../Linux%EA%B8%B0%EC%B4%88%26%EB%AA%85%EB%A0%B9%EC%96%B4%EC%A0%95%EB%A6%AC.md#redirection)
