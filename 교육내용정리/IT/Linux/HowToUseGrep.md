# How to use grep command option in linux

### What is grep
- Find row what having pattern in Output or selected file.

### How to use 'and' in grep
1. Use grep twice
```bash
cat test.txt | grep pattern1 | grep pattern2
```
2. Use option -E
```bash
# order exists
cat test.txt | grep -E "pattern1.*pattern2"
# In any order
cat test.txt | grep -E "pattern1.*pattern2|pattern2.*pattern1"
```
### How to use 'or' in grep
1. Use option -e
```bash
cat test.txt | grep -e pattern1 -e pattern2
```
2. Use option -E
```bash
cat test.txt | grep -E "pattern1|pattern2"
```

### How to use 'not' in grep
1. Use option -v
```bash
cat test.txt | grep -v pattern
```