# 2022.08.19 주간미팅 교육 자료 정리


### What is Protein?
- Amino Acid이 Peptide bond로 결합된 물질
    - peptide bond : 두 Amino Acid가 결합(CO-NH). 10개 이상의 결합 다발을 Polypeptide라고 한다.
- 효소, 호르몬, 주요 생체기능 수행
- 근육 등 체조직 구성

### RNA & DNA
- DNA : 유전자의 기초 물질 (유전정보를 갖는다.)
    - A, G, C, T
- Gene : 개체들의 특징을 결정하는 정보를 갖는다. 단백질을 제작하는데 필요한 정보를 가진 DNA단편.
- Chromosome 염색체 : DNA-Histon Complex로 이루어진 세포 내 구조
- Genome 유전체 : 한 개체 또는 세포가 갖고 있는 유전정보 총합.
- RNA : DNA가 단백질을 합성하는 과정에서 작용.
    - A, G, C, U
    - rRNA : 리보솜을 구성하는 RNA
    - tRNA : 안티코돈을 갖고 있어, 특정 Amino Acid가 있는 RNA
    - mRNA : 단백질 생산에 관여하는 RNA


### Central Dogma
- 유전정보를 갖는 DNA > 실질적 기능을 담당하는 단백질. 일련의 과정.
- 생명과학 연구의 기본적이 법칙(바이러스와 같은 예외사항도 있다.)
1. Transcription : DNA > RNA 정보 전달
    1. initiaion(개시) : 풀린 DNA 아랫 가닥의 promoter 부위에 RNA 중합효소가 부착, 전사인자들의 영향을 받아 시작
    2. elongation(신장) : RNA 중합효소가 3' > 5' 방향으로 이동하며 preRNA 형성
    3. termination(종결) : 종결자 서열에 도달하면 합성된 RNA 전사체가 떨어져나오며 종료.
    - preRNA는 유전정보를 담고 있는 exon과 intron으로 구성. RNA Splicing 과정을 통해 intron부분이 제거된 mRNA 형성.
2. Translation : mRNA의 정보에 따라 Amino Acid가 순서대로 결합해 단백질 생성
    1. initiaion(개시) : 리보솜의 작은 소단위가 mRNA의 개시암호(AUG)를 찾아 결합. MET를 가진 tRNA가 결합하면 큰 소단위가 부착.
    2. elongation(신장) : 두 번째 Amino Acid를 운반하는 tRNA가 mRNA의 두 번째 코돈에 부착. Amino Acid 사이에 peptide bond가 형성. tRNA 방출, 이 과정을 반복하여 Polypeptide 형성
    3. termination(종결) : mRNA의 중지코돈(UGA,UAG,UAA)에 도착하면 방출인자가 결합, 마지막 tRNA 방출, 리보솜해체, Polypeptide 방출.

### PTM(Post Translational modification)
- Function group 또는 protein에 공유결합이 첨가되거나 peptide bond가 분해되어 protein의 구조가 변하는 현상.
- 구조 변화를 통해 protein의 다양성을 증대시키는 중요한 메커니즘이다.

```
[DNA]

▼ transcription

[preRNA]

▼ RNA Splicing

[mRNA]

▼ translation

[Amino Acid (Polypeptide)]
```