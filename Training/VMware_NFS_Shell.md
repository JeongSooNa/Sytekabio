# VMware Linux NFS 가상환경에서 Shell 명령어를 이용한 스크립트 실행, 파싱

***
### grep & awk
- file, output의 행/열 단위 추출.
1. grep
    - grep은 '|'를 쓰지만 '&(and)'의 의미를 같는다!
2. awk
    - $0 : 레코드, 행 ex) length($0)은 각 행의 길이(띄어쓰기 포함)를 의미.
    - $n : 필드, 띄어쓰기로 구분된 각 열(컬럼, 변수, 파라미터)
    - awk '_pattern_ {_action_}' file_name 으로 구성
    - action을 생략하면 기본 action인 print 실행

***
### test grep & awk
1. raw data (test.txt)
> cat test.txt

![jpg](./img/nfs_shell_1.PNG)

2. grep
> cat test.txt | grep 1

![jpg](./img/nfs_shell_2.PNG)

3. awk pring all $n
> awk '{print}' test.txt  
> awk '{print $a}' test.txt

![jgp](./img/nfs_shell_6.PNG)

4. awk $1
> awk '{print $1}' test.txt

![jpg](./img/nfs_shell_3.PNG)

5. awk $2
> awk '{print $2}' test.txt

![jpg](./img/nfs_shell_4.PNG)

6. awk $0 length, print $n
    - 레코드의 길이(행의 길이) 가 6보다 큰 $1, $2 출력 (title 제외)
    > awk 'length($0) > 6 {print $1, $2}' test.txt | grep -v title
    - 레코드의 길이(행의 길이) 가 7보다 큰 $1, $2 출력 (title 제외)
    > awk 'length($0) > 7 {print $1, $2}' test.txt | grep -v title

![jpg](./img/nfs_shell_5.PNG)

7. awk action없이 각 $0 조건 print
> awk '/0/' test.txt

- awk로 열 외에 행 별로 조건을 줘 grep처럼 사용할 수 있다.

![jpg](./img/nfs_shell_7.PNG)

8. awk "text"(문자열) 포함하여 출력
> awk '{print "name : "$1, "/ value : "$2}' test.txt

![jpg](./img/nfs_shell_11.PNG)

9. 연산, 비교, 조건문, 반복문, 문자열 길이, 최대,최솟값 출력 등 다양한 로직 구현
- 해야할 것!

***
### bash
- Shell 명령어를 통해 스크립트를 다양하게 실행. (.sh)
> bash shell.sh

![jpg](./img/nfs_shell_8.PNG)

- 일반 명령어와 다르게 mounted 디렉토리가 색이 칠해지지 않는다.

> bash shell.sh | grep root

![jpg](./img/nfs_shell_9.PNG)

- na 디렉토리에서 실행했을 때 na 디렉토리, 그룹에 속해있지 않고 root에 속해있다는 것은 다른 서버에 mount 했다는 뜻!?!?

- NFS-server에서 실행하면 server의 경로 기준으로 실행되어 다른 결과가 나온다.

![jpg](./img/nfs_shell_10.PNG)


## Reference
- [awk 명령어](https://recipes4dev.tistory.com/171)